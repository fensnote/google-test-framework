# google测试框架

#### 介绍
谷歌测试框架，对其做了一些修改；做了一个测试代码的通用Makefile.inc脚本；
这样只需要写测试代码和配置测试代码的Makefile里面对应的文件即可。

#### 软件架构
gtest-1.6.0: google gtest测试框架源码

test_gtest：测试demo的代码与makefile

#### 安装教程

##### 编译安装gtest：

```` bash
cd gtest-1.6.0
./configure    #生成makefile
make -j4
sudo make install
````

库文件默认是安装在/usr/lib下面，头文件安装在/usr/include下面。

#### 使用说明

##### 目录介绍

````c
.
├── gtest.pdf -----------------------gtest文档
├── test ----------------------------单元测试目录
│   ├── Makefile --------------------测试代码的makefile
│   ├── Makefile.inc ----------------公共makefile
│   ├── obj  ------------------------编译过程文件.o存放目录
│   ├── test ------------------------测试框架代码编译结果，可执行文件；
│   └── unit_test -------------------单元测试框架demo代码
│       ├── test_c.cpp
│       ├── test_gtest.cpp
│       ├── test_paraList.cpp
│       ├── test_testCase.cpp
│       ├── test_test.cpp
│       └── test_testSuit.cpp
├── test_c.c  -----------------------被测试代码
├── test.cc   -----------------------被测试代码
├── test_c.h  -----------------------被测试代码
├── test.cpp  -----------------------被测试代码
└── test.h    -----------------------被测试代码

````



可以进入demo代码目录编译测试：

````bash
cd test_gtest/test
make 
./test
````

运行结果：

````bash
Running main() from gtest_main.cc
-----------------------start test-----------------------
[==========] Running 24 tests from 9 test cases.
[----------] Global test environment set-up.
[----------] 2 tests from TestSuitTest
------------------TestSuitTest----set up
================================================
[ RUN      ] TestSuitTest.Test1
[       OK ] TestSuitTest.Test1 (0 ms)
================================================
[ RUN      ] TestSuitTest.Test2
[       OK ] TestSuitTest.Test2 (0 ms)
------------------TestSuitTest----Tear down 
[----------] 2 tests from TestSuitTest (0 ms total)

[----------] 2 tests from TestCaseTest
================================================
[ RUN      ] TestCaseTest.ZeroEqual
------------------testCase----set up
-------------------testCase---TearDown
[       OK ] TestCaseTest.ZeroEqual (0 ms)
================================================
[ RUN      ] TestCaseTest.OneEqual
------------------testCase----set up
-------------------testCase---TearDown
[       OK ] TestCaseTest.OneEqual (0 ms)
[----------] 2 tests from TestCaseTest (0 ms total)

[----------] 2 tests from FooTest
================================================
[ RUN      ] FooTest.ZeroEqual
----------------------set up
----------------------TearDown
[       OK ] FooTest.ZeroEqual (0 ms)
================================================
[ RUN      ] FooTest.OneEqual
----------------------set up
----------------------TearDown
[       OK ] FooTest.OneEqual (0 ms)
[----------] 2 tests from FooTest (0 ms total)

[----------] 3 tests from test
================================================
[ RUN      ] test.sum_ok
[       OK ] test.sum_ok (0 ms)
================================================
[ RUN      ] test.sum_0
[       OK ] test.sum_0 (0 ms)
================================================
[ RUN      ] test.sum_negtive
sum(3,-6) = -3
[       OK ] test.sum_negtive (0 ms)
[----------] 3 tests from test (0 ms total)

[----------] 1 test from catString
================================================
[ RUN      ] catString.catString_null
/mnt/hgfs/share_VM/mycode/gtest/test_gtest/test/unit_test/test_test.cpp:31: Failure
Expected: catString(__null, __null) throws an exception of type char*.
  Actual: it throws a different type.
[  FAILED  ] catString.catString_null (0 ms)
[----------] 1 test from catString (0 ms total)

[----------] 2 tests from textMax
================================================
[ RUN      ] textMax.EXPECT_PRED2_false
/mnt/hgfs/share_VM/mycode/gtest/test_gtest/test/unit_test/test_test.cpp:38: Failure
textMax(m, n) evaluates to false, where
m evaluates to 5
n evaluates to 6
[  FAILED  ] textMax.EXPECT_PRED2_false (0 ms)
================================================
[ RUN      ] textMax.EXPECT_PRED2_true
[       OK ] textMax.EXPECT_PRED2_true (0 ms)
[----------] 2 tests from textMax (0 ms total)

[----------] 1 test from AssertSumTest
================================================
[ RUN      ] AssertSumTest.HandleFail
/mnt/hgfs/share_VM/mycode/gtest/test_gtest/test/unit_test/test_test.cpp:61: Failure
3 和 6 相加得：9 而不是：2
[  FAILED  ] AssertSumTest.HandleFail (0 ms)
[----------] 1 test from AssertSumTest (0 ms total)

[----------] 1 test from test_c
================================================
[ RUN      ] test_c.sum_c_ok
[       OK ] test_c.sum_c_ok (0 ms)
[----------] 1 test from test_c (0 ms total)

[----------] 10 tests from TrueReturn/IsPrimeParamTest
================================================
[ RUN      ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/0
[       OK ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/0 (0 ms)
================================================
[ RUN      ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/1
[       OK ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/1 (0 ms)
================================================
[ RUN      ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/2
[       OK ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/2 (0 ms)
================================================
[ RUN      ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/3
[       OK ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/3 (0 ms)
================================================
[ RUN      ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/4
[       OK ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/4 (0 ms)
================================================
[ RUN      ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/5
[       OK ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/5 (0 ms)
================================================
[ RUN      ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/6
[       OK ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/6 (0 ms)
================================================
[ RUN      ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/7
[       OK ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/7 (0 ms)
================================================
[ RUN      ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/8
[       OK ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/8 (0 ms)
================================================
[ RUN      ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/9
[       OK ] TrueReturn/IsPrimeParamTest.HandleTrueReturn/9 (0 ms)
[----------] 10 tests from TrueReturn/IsPrimeParamTest (1 ms total)

[----------] Global test environment tear-down
--------------------------RESULT--------------------------
[==========] 24 tests from 9 test cases ran. (1 ms total)
[  PASSED  ] 21 tests.
[  FAILED  ] 3 tests, listed below:
[  FAILED  ] catString.catString_null
[  FAILED  ] textMax.EXPECT_PRED2_false
[  FAILED  ] AssertSumTest.HandleFail

 3 FAILED TESTS

````

#### 微信公众号

![](https://img-blog.csdnimg.cn/20210503163008786.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3d1cXVhbl8xMjMw,size_16,color_FFFFFF,t_70#pic_center)