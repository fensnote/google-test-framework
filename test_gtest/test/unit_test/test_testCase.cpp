#include "gtest/gtest.h"

/*
TestCase事件 
TestCase事件是挂在每个案例执行前后的，实现方式实现一个类，不过需要实现的是SetUp方法和TearDown方法：

1. SetUp()方法在每个TestCase之前执行
2. TearDown()方法在每个TestCase之后执行
*/
namespace 
{

// The fixture for testing class Foo.

class TestCaseTest : public testing::Test 

{

 protected:

  TestCaseTest() 

  {

    // You can do set-up work for each test here.

  }

  virtual ~TestCaseTest() 

{

    // You can do clean-up work that doesn't throw exceptions here.

  }

  virtual void SetUp() 

{

    // Code here will be called immediately after the constructor (right
	std::cout<<"------------------testCase----set up"<<std::endl;
    // before each test).

  }

  virtual void TearDown() 

{

    // Code here will be called immediately after each test (right
	std::cout<<"-------------------testCase---TearDown"<<std::endl;
    // before the destructor).

  }

  // Objects declared here can be used by all tests in the test case for Foo.

};

 

// Tests that the Foo::Bar() method does Abc.

TEST_F(TestCaseTest, ZeroEqual) 

{

  EXPECT_EQ(0,0);

}
// Tests that Foo HiHiHi.

TEST_F(TestCaseTest, OneEqual) 
{

  EXPECT_EQ(1,1);

}

 

}  // namespace