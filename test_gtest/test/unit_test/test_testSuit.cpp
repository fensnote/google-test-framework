#include "gtest/gtest.h"
/*
TestSuite事件
我们需要写一个类，继承testing::Test，然后实现两个静态方法

1. SetUpTestCase() 方法在第一个TestCase之前执行

2. TearDownTestCase() 方法在最后一个TestCase之后执行
*/


namespace 
{
class TestSuitTest : public testing::Test {

 protected:

  static void SetUpTestCase() {

    //shared_resource_ = new ;
	std::cout<<"------------------TestSuitTest----set up"<<std::endl;

  }

  static void TearDownTestCase() {

    //delete shared_resource_;
	std::cout<<"------------------TestSuitTest----Tear down "<<std::endl;

    //shared_resource_ = NULL;

  }
};
  // Some expensive resource shared by all tests.
  
  //static T* shared_resource_;

TEST_F(TestSuitTest, Test1)
{
    // you can refer to shared_resource here 
	
}
TEST_F(TestSuitTest, Test2)
{
    // you can refer to shared_resource here 
	
}  
  
}