#include "gtest/gtest.h"
#include "test.h"
#include <iostream>


TEST(test, sum_ok) {
	
	
	EXPECT_EQ(2, sum(1,1));
	//sleep(1);

}
TEST(test, sum_0) {
	
	EXPECT_EQ(6, sum(1, 5));
	//EXPECT_TRUE();
	
}

TEST(test, sum_negtive) {

	EXPECT_EQ(-3,sum(3,-6));
	std::cout<<"sum(3,-6) = "<<sum(3,-6)<<std::endl;
}

TEST(catString, catString_null) {

	char str[128]={0};
	
	EXPECT_ANY_THROW(catString(str, NULL));
    EXPECT_THROW(catString(NULL, NULL), char*);
}
//测试 能够输出传入的参数是什么，并且可以传入更多参数
//测试返回值为布尔型 EXPECT_PRED2(pred2, val1, val2);	pred2(val1, val2) returns true
TEST(textMax, EXPECT_PRED2_false)
{
    int m = 5, n = 6;
    EXPECT_PRED2(textMax, m, n);
}
TEST(textMax, EXPECT_PRED2_true)
{
    int m = 5, n = 6;    
	EXPECT_PRED2(textMax, n, m);
}
/*

Fatal assertion	                                  Nonfatal assertion	                            Verifies
ASSERT_PRED_FORMAT1(pred_format1, val1);`	      EXPECT_PRED_FORMAT1(pred_format1, val1);	        pred_format1(val1) is successful
ASSERT_PRED_FORMAT2(pred_format2, val1, val2);	  EXPECT_PRED_FORMAT2(pred_format2, val1, val2);	pred_format2(val1, val2) is successful
可以自定义输出格式的测试
*/
testing::AssertionResult AssertSum(const char* m_expr, const char* n_expr, const char* k_expr, int m, int n, int k) {
    if (sum(m, n) == k)
        return testing::AssertionSuccess();
    testing::Message msg;
    msg << m_expr << " 和 " << n_expr << " 相加得：" << sum(m, n) << " 而不是：" << k_expr;
    return testing::AssertionFailure(msg);
}
TEST(AssertSumTest, HandleFail)
{
    EXPECT_PRED_FORMAT3(AssertSum, 3, 6, 2);
}







