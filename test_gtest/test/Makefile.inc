#gtest公共部分makefile

#INC_DIR 

#修改编译器
ARCH ?=
CC=$(ARCH)gcc
CPP=$(ARCH)g++
AR=$(ARCH)ar
AR=$(ARCH)ar
STRIP=$(ARCH)strip


OBJS += $(SRC_OBJS) $(TEST_OBJS)

LDFLAGS := -lpthread
LDFLAGS += -lgtest -lgtest_main

CFLAGS  := -Wall 
CFLAGS  += $(foreach dir,$(INC_DIR),-I$(dir))
LDFLAGS += $(foreach lib,$(LIB_PATH),-L$(lib))


all:$(TARGET)
$(TARGET): $(OBJS)
	$(CPP) $(OBJS) -o $@ $(CFLAGS) $(LDFLAGS)
	$(STRIP) $(TARGET)

$(TEST_OBJ_DIR)/%.o: $(TEST_SRC_DIR)/%.cpp
	$(CPP) $(CFLAGS) -c $< -o $@
	@echo "$@"

$(SRC_OBJ_DIR)/%.o: $(SRC_OBJ_DIR)/%.cpp
	$(CPP) $(CFLAGS) -c $< -o $@
	@echo "$@"

$(SRC_OBJ_DIR)/%.o: $(SRC_OBJ_DIR)/%.cc
	$(CPP) $(CFLAGS) -c $< -o $@
	@echo "$@"
	
$(SRC_OBJ_DIR)/%.o: $(SRC_OBJ_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@
	@echo "$@"

print:
	@echo "-----------------------------------------------------------------------"
	@echo "SRC file dir: $(SRC_OBJ_DIR)"
	@echo "-------------------------------src file list---------------------------"
	@echo $(SRC)
	@echo "-------------------------------obj file list---------------------------"
	@echo $(OBJS)


clean:
	-rm $(TEST_OBJ_DIR)/*.o $(TARGET) $(SRC_OBJS)

